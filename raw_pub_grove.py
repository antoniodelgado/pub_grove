#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import time
import json
from GreenPonik_SHT40.SHT40 import SHT40
from grove.adc import ADC
import sys

if __name__ == "__main__":
    sht = SHT40()
    adc = ADC(address=8) # sudo i2cdetect -y 1
    while True:
        data = sht.read_sht40()
        if data:
            print("temperature: %.2f / humidity: %.2f" % (data[0], data[1]))
        for channel in range(0, 32):
            sys.stdout.write(f"{adc.read(channel)}\t")
        print()
        time.sleep(.1)